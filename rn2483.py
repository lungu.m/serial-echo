#!/usr/bin/python3
import serial
import time
from _thread import *
from functools import partial
import random
import mqtt
from datetime import datetime


def set_key(command, key_len):
    if len(command) != 4 or len(command[3]) != key_len:
        return False
    try:
        int(command[3], 16)
    except ValueError:
        return False
    return True


def set_channel_param(command, num_of_params):
    if len(command) != num_of_params:
        return False

    try:
        ch_id = int(command[4])
        int(command[5])
        if num_of_params == 7:
            int(command[6])
    except ValueError:
        return False

    if (not (3 <= ch_id <= 15)) and (command[3] != 'dcycle'):
        return False

    return True


def set_param(command, num_of_params, min_val, max_val):
    if len(command) != num_of_params:
        return False

    try:
        cnt = int(command[3])
    except ValueError:
        return False

    if not (min_val <= cnt <= max_val):
        return False

    return True


def get_toa(sf, bw, cr, data):
    import math
    payload = len(data)/2 + 13
    de = 0 if sf < 10 else 1
    t_sym = ((2 ** sf) / bw)
    t_preamble = (8 + 4.25) * t_sym
    payload_sym = 8 + max(math.ceil((8 * payload - 4 * sf + 28 + 16)/(4 * (sf - 2 * de))) * (cr + 1), 0)
    t_payload = payload_sym * t_sym

    return t_preamble + t_payload


class RN243:
    run = True
    ok_response = 'ok'
    busy_response = 'busy'
    invalid_param_response = 'invalid_param'
    err_response = 'err'
    mac_paused_response = 'mac_paused'
    no_free_ch_response = 'no_free_ch'
    not_joined_response = 'not_joined'
    invalid_data_length = 'invalid_data_len'
    tx_ok_response = 'mac_tx_ok'
    rx_response = 'mac_rx'
    keys_not_init_response = 'keys_not_init'
    accepted_response = 'accepted'
    drs = {0: {'sf': 12, 'bw': 125, 'max': 51},
           1: {'sf': 11, 'bw': 125, 'max': 51},
           2: {'sf': 10, 'bw': 125, 'max': 51},
           3: {'sf': 9, 'bw': 125, 'max': 115},
           4: {'sf': 8, 'bw': 125, 'max': 222},
           5: {'sf': 7, 'bw': 125, 'max': 222},
           6: {'sf': 7, 'bw': 250, 'max': 222},
           7: {'sf': 0, 'bw': 0, 'max': 222}
           }
    cr = 4

    appEUI = ['', '']
    appKey = ['', '']
    nwksKey = ['', '']
    appsKey = ['', '']
    devEUI = ['00112233445566778899AABBCCDDEEFF', '00112233445566778899AABBCCDDEEFF']
    devAddr = ['00000000', '00000000']
    hwEUI = '0123456789ABCDEF'
    sys_ver = 'RN2483 X.Y.Z MMM DD YYYY HH:MM:SS'
    freq = '868'
    joined = False
    mac_paused = False
    silent = False
    ar = False
    dr = [0, 0]
    dl_ctr = [0, 0]
    ul_ctr = [0, 0]
    pwr_idx = [5, 5]
    re_tx = [0, 0]
    rx_delay1 = [1000, 1000]
    rx_delay2 = 2000
    sync = ['34', '34']
    channels = {
        0: {'freq': 868100000, 'dc': 302, 'min_dr': 0, 'max_dr': 5, 'status': True, 'next_tx': 0},
        1: {'freq': 868300000, 'dc': 302, 'min_dr': 0, 'max_dr': 5, 'status': True, 'next_tx': 0},
        2: {'freq': 868500000, 'dc': 302, 'min_dr': 0, 'max_dr': 5, 'status': True, 'next_tx': 0},
        3: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
            'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        4: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
            'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        5: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
            'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        6: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
            'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        7: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
            'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        8: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
            'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        9: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
            'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        10: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
             'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        11: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
             'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        12: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
             'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        13: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
             'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        14: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
             'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        15: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
             'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
        16: {'freq': 869525000, 'dc': 65535, 'min_dr': 0, 'max_dr': 5, 'status': True, 'freq_s': 869525000, 'dc_s': 0, 'min_dr_s': 0,
             'max_dr_s': 5, 'status_s': True}
    }

    mqtt_client = None

    def __init__(self, port, speed):
        self.comport = serial.Serial(port, speed, timeout=.2)
        self.mqtt_client = mqtt.LoRaWANMQTT('127.0.0.1')
        start_new_thread(self.reception_loop, ())

    def receive_command(self):
        buffer = ''
        while True:
            if self.comport.inWaiting():
                rx_char = str(self.comport.read_until(b'#'), 'utf-8', errors='ignore')
                buffer += rx_char
            else:
                break

        buffer.strip('\r\n')
        print('Rx: {}'.format(buffer))
        return buffer

    def send_response(self, response):
        print('Tx: {}'.format(response))
        try:
            self.comport.write((response + '\r\n').encode())
        except serial.SerialException:
            pass

    def reception_loop(self):
        while self.run:
            if self.comport.inWaiting():
                command = self.receive_command()
                self.main_switch(command)
            self.mqtt_client.proc(0.01)
            # time.sleep(0.01)

    def main_switch(self, command):
        split_command = str.split(command)
        switcher = {
            'sys': partial(self.sys_switch, split_command),
            'mac': partial(self.mac_switch, split_command),
            'radio': partial(self.radio_switch, split_command)
        }
        func = switcher.get(split_command[0], 'Invalid command')

        if isinstance(func, str):
            self.send_response(self.invalid_param_response)
        else:
            func()

    def sys_switch(self, command: list):
        switcher = {
            'sleep': partial(self.sys_sleep, command),
            'reset': partial(self.sys_reset, command),
            'factoryRESET': partial(self.sys_factory_reset, command),
            'get': partial(self.sys_get_switcher, command)

        }
        func = switcher.get(command[1], 'Invalid command')

        if isinstance(func, str):
            self.send_response(self.invalid_param_response)
        else:
            func()

    def sys_get_switcher(self, command: list):
        switcher = {
            'ver': partial(self.sys_get_ver, command),
            'hweui': partial(self.sys_get_hweui, command)
        }

        func = switcher.get(command[2], 'Invalid command')

        if isinstance(func, str):
            self.send_response(self.invalid_param_response)
        else:
            func()

    def mac_switch(self, command: list):
        switcher = {
            'reset': partial(self.mac_reset, command),
            'tx': partial(self.mac_tx, command),
            'join': partial(self.mac_join, command),
            'save': partial(self.mac_save, command),
            'forceENABLE': partial(self.mac_force_enable, command),
            'pause': partial(self.mac_pause, command),
            'resume': partial(self.mac_resume, command),
            'set': partial(self.mac_set_switch, command),
            'get': partial(self.mac_get_switch, command)
        }

        func = switcher.get(command[1], 'Invalid command')

        if isinstance(func, str):
            self.send_response(self.invalid_param_response)
        else:
            func()

    def mac_set_switch(self, command: list):
        switcher = {
            'appkey': partial(self.mac_set_appkey, command),
            'appskey': partial(self.mac_set_appskey, command),
            'ar': partial(self.mac_set_ar, command),
            'ch': partial(self.mac_ch_switch, command),
            'devaddr': partial(self.mac_set_devaddr, command),
            'deveui': partial(self.mac_set_deveui, command),
            'dnctr': partial(self.mac_set_dl_counter, command),
            'dr': partial(self.mac_set_dr, command),
            'nwkskey': partial(self.mac_set_nwkskey, command),
            'pwridx': partial(self.mac_set_pwr_idx, command),
            'retx': partial(self.mac_set_re_tx, command),
            'rx2': partial(self.mac_set_rx2, command),
            'rxdelay1': partial(self.mac_set_rx_delay1, command),
            'sync': partial(self.mac_set_sync, command),
            'upctr': partial(self.mac_set_ul_counter, command)
        }

        func = switcher.get(command[2], 'Invalid command')

        if isinstance(func, str):
            self.send_response(self.invalid_param_response)
        else:
            func()

    def mac_get_switch(self, command: list):
        switcher = {
            'ar': partial(self.mac_get_ar, command),
            'ch': partial(self.mac_ch_switch, command),
            'devaddr': partial(self.mac_get_devaddr, command),
            'deveui': partial(self.mac_get_deveui, command),
            'dnctr': partial(self.mac_get_dncr, command),
            'dr': partial(self.mac_get_dr, command),
            'pwridx': partial(self.mac_get_pwridx, command),
            'retx': partial(self.mac_get_re_tx, command),
            'rx2': partial(self.mac_get_rx2, command),
            'rxdelay1': partial(self.mac_get_rx_delay1, command),
            'rxdelay2': partial(self.mac_get_rx_delay2, command),
            'sync': partial(self.mac_get_sync, command),
            'upctr': partial(self.mac_get_upctr, command)
        }

        func = switcher.get(command[2], 'Invalid command')

        if isinstance(func, str):
            self.send_response(self.invalid_param_response)
        else:
            func()

    def mac_ch_switch(self, command: list):
        if command[1] == 'get':
            command[3] = command[3] + '_g'

        switcher = {
            'freq': partial(self.mac_set_ch_freq, command),
            'dcycle': partial(self.mac_set_ch_dcycle, command),
            'drrange': partial(self.mac_set_ch_drrange, command),
            'status': partial(self.mac_set_ch_status, command),
            'freq_g': partial(self.mac_get_ch_param, command, 'freq'),
            'dcycle_g': partial(self.mac_get_ch_param, command, 'dc'),
            'drrange_g': partial(self.mac_get_ch_param, command, 'dr'),
            'status_g': partial(self.mac_get_ch_param, command, 'status')
        }

        func = switcher.get(command[3], 'Invalid command')

        if isinstance(func, str):
            self.send_response(self.invalid_param_response)
        else:
            func()

    def radio_switch(self, command: list):
        print('Radio commands not Implemented')
        self.send_response(self.err_response)

    def stop_loop(self):
        self.run = False
        self.comport.close()

    def sys_sleep(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return
        try:
            t = int(command[2])
        except ValueError:
            self.send_response(self.invalid_param_response)
            return
        if t < 100 or t > 4294967296:
            self.send_response(self.invalid_param_response)
            return
        time.sleep(t/1000)
        self.send_response(self.ok_response)

    def sys_reset(self, command):
        if len(command) != 2:
            self.send_response(self.invalid_param_response)
            return

        self.save_values(0, 1)

        self.joined = False
        self.mac_paused = False
        self.silent = False

        d = random.randint(500, 1500)
        time.sleep(d/1000)
        self.send_response(self.sys_ver)

    def sys_factory_reset(self, command):
        if len(command) != 2:
            self.send_response(self.invalid_param_response)
            return
        d = random.randint(500, 1500)
        time.sleep(d / 1000)
        self.appEUI = ['', '']
        self.appKey = ['', '']
        self.nwksKey = ['', '']
        self.appsKey = ['', '']
        self.devEUI = ['00112233445566778899AABBCCDDEEFF', '00112233445566778899AABBCCDDEEFF']
        self.devAddr = ['00000000', '00000000']
        self.dl_ctr = [0, 0]
        self.ul_ctr = [0, 0]
        self.dr = [0, 0]
        self.pwr_idx = [5, 5]
        self.re_tx = [0, 0]
        self.rx_delay1 = [1000, 1000]
        self.joined = False
        self.mac_paused = False
        self.silent = False
        self.ar = False
        self.channels = {
            0: {'freq': 868100000, 'dc': 302, 'min_dr': 0, 'max_dr': 5, 'status': True, 'next_tx': 0},
            1: {'freq': 868300000, 'dc': 302, 'min_dr': 0, 'max_dr': 5, 'status': True, 'next_tx': 0},
            2: {'freq': 868500000, 'dc': 302, 'min_dr': 0, 'max_dr': 5, 'status': True, 'next_tx': 0},
            3: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            4: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            5: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            6: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            7: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            8: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            9: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            10: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                 'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            11: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                 'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            12: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                 'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            13: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                 'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            14: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                 'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            15: {'freq': 0, 'dc': 65535, 'min_dr': 15, 'max_dr': 15, 'status': False, 'next_tx': 0, 'freq_s': 0, 'dc_s': 65535,
                 'min_dr_s': 15, 'max_dr_s': 15, 'status_s': False},
            16: {'freq': 869525000, 'dc': 65535, 'min_dr': 0, 'max_dr': 5, 'status': True, 'freq_s': 869525000, 'dc_s': 0,
                 'min_dr_s': 0, 'max_dr_s': 5, 'status_s': True}
        }
        self.send_response(self.sys_ver)

    def sys_get_ver(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return
        self.send_response(self.sys_ver)

    def sys_get_hweui(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return
        self.send_response(self.hwEUI)

    def mac_reset(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        if command[2] == '868' or command[2] == '433':
            self.freq = command[2]
            self.send_response(self.ok_response)
            return
        else:
            self.send_response(self.invalid_param_response)

    def check_length(self, data):
        data_len = len(data)/2
        if data_len > self.drs[self.dr[0]]['max']:
            return False
        return True

    def select_channel(self, toa):
        transmitted = -1
        for i in range(15):
            if self.channels[i]['status'] is True:
                current_time = time.time()
                if self.channels[i]['next_tx'] < current_time:
                    dc_pct = 1 / (self.channels[i]['dc'] + 1)
                    self.channels[i]['next_tx'] = time.time() + 0 if self.channels[i]['dc'] == 65535 else ((toa / dc_pct) - toa)/1000
                    transmitted = i
                    break
        return transmitted

    def mac_tx(self, command):
        if len(command) != 5:
            self.send_response(self.invalid_param_response)
            return
        if not (command[2] == 'cnf' or command[2] == 'uncnf'):
            self.send_response(self.invalid_param_response)
            return
        try:
            p = int(command[3])
        except ValueError:
            self.send_response(self.invalid_param_response)
            return

        if not (1 < p < 223):
            self.send_response(self.invalid_param_response)
            return

        try:
            int(command[4], 16)
        except ValueError:
            self.send_response(self.invalid_param_response)
            return

        if not self.joined:
            self.send_response(self.not_joined_response)
            return

        if self.mac_paused:
            self.send_response(self.mac_paused_response)
            return

        if not self.check_length(command[4]):
            self.send_response(self.invalid_data_length)
            return

        self.send_response(self.ok_response)

        toa = get_toa(self.drs[self.dr[0]]['sf'], self.drs[self.dr[0]]['bw'], self.cr, command[4])

        tx_ch = self.select_channel(toa)
        if tx_ch < 0:
            self.send_response(self.no_free_ch_response)
            return

        msg = {'msg': command[4], 'freq': self.channels[tx_ch]['freq'], 'port': p, 'dr': self.dr[0], 'toa': toa}

        time.sleep(toa/1000)

        self.mqtt_client.send_msg(msg)

        if command[2] == 'uncnf':
            self.send_response(self.tx_ok_response)
        else:
            rec_data = 'RECV({})'.format((datetime.now()).strftime("%H:%M:%S.%f"))
            toa = get_toa(self.drs[self.dr[0]]['sf'], self.drs[self.dr[0]]['bw'], self.cr, rec_data)
            time.sleep((self.rx_delay1[0] + toa) / 1000)
            self.send_response('{} {} {}'.format(self.rx_response, random.randint(1, 223),
                                                 str(rec_data.encode('utf-8').hex()).upper()))

    def mac_join(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        if not (command[2] == 'otaa' or command[2] == 'abp'):
            self.send_response(self.invalid_param_response)
            return

        if self.mac_paused:
            self.send_response(self.mac_paused)
            return

        if (command[2] == 'otaa') and (self.devEUI[0] == "" or self.appEUI[0] == "" or self.appsKey[0] == ""):
            self.send_response(self.keys_not_init_response)
            return

        if (command[2] == 'abp') and (self.nwksKey[0] == "" or self.appsKey[0] == ""):
            self.send_response(self.keys_not_init_response)
            return

        self.joined = True
        self.send_response(self.accepted_response)

    def save_values(self, first, second):
        self.appEUI[first] = self.appEUI[second]
        self.appKey[first] = self.appKey[second]
        self.nwksKey[first] = self.nwksKey[second]
        self.appsKey[first] = self.appsKey[second]
        self.devEUI[first] = self.devEUI[second]
        self.dl_ctr[first] = self.dl_ctr[second]
        self.ul_ctr[first] = self.ul_ctr[second]
        self.dr[first] = self.dr[second]
        self.pwr_idx[first] = self.pwr_idx[second]
        self.rx_delay1[first] = self.rx_delay1[second]
        self.re_tx[first] = self.re_tx[second]
        self.sync[first] = self.sync[second]

        pre_1 = '_s'
        pre_2 = ''

        if first < second:
            pre_1 = ''
            pre_2 = '_s'

        for key in self.channels:
            if key < 3:
                continue
            self.channels[key]['freq' + pre_1] = self.channels[key]['freq' + pre_2]
            self.channels[key]['dc' + pre_1] = self.channels[key]['dc' + pre_2]
            self.channels[key]['min_dr' + pre_1] = self.channels[key]['min_dr' + pre_2]
            self.channels[key]['max_dr' + pre_1] = self.channels[key]['max_dr' + pre_2]
            self.channels[key]['status' + pre_1] = self.channels[key]['status' + pre_2]

    def mac_save(self, command):
        if len(command) != 2:
            self.send_response(self.invalid_param_response)
            return

        self.save_values(1, 0)
        self.send_response(self.ok_response)

    def mac_force_enable(self, command):
        if len(command) != 2:
            self.send_response(self.invalid_param_response)
            return

        self.silent = False
        self.send_response(self.ok_response)

    def mac_pause(self, command):
        if len(command) != 2:
            self.send_response(self.invalid_param_response)
            return

        self.mac_paused = True
        self.send_response('4294967295')

    def mac_resume(self, command):
        if len(command) != 2:
            self.send_response(self.invalid_param_response)
            return

        self.mac_paused = False
        self.send_response(self.ok_response)

    def mac_set_appkey(self, command):
        if not set_key(command, 32):
            self.send_response(self.invalid_param_response)
            return

        self.appKey[0] = command[3]
        self.send_response(self.ok_response)

    def mac_set_appskey(self, command):
        if not set_key(command, 32):
            self.send_response(self.invalid_param_response)
            return

        self.appsKey[0] = command[3]
        self.send_response(self.ok_response)

    def mac_set_ar(self, command):
        if len(command) != 4:
            self.send_response(self.invalid_param_response)
            return

        if not (command[3] == 'on' or command[3] == 'off'):
            self.send_response(self.invalid_param_response)
            return

        self.ar = True if command[3] == 'on' else False
        self.send_response(self.ok_response)

    def mac_set_ch_freq(self, command):
        if not set_channel_param(command, 6):
            self.send_response(self.invalid_param_response)
            return

        ch_id = int(command[4])
        ch_freq = int(command[5])

        if not (433050000 <= ch_freq <= 434790000) and not (863000000 <= ch_freq <= 870000000):
            self.send_response(self.invalid_param_response)
            return

        self.channels[ch_id]['freq'] = ch_freq
        self.send_response(self.ok_response)

    def mac_set_ch_dcycle(self, command):
        if not set_channel_param(command, 6):
            self.send_response(self.invalid_param_response)
            return

        ch_id = int(command[4])
        ch_dc = int(command[5])

        if not (0 <= ch_dc <= 65535):
            self.send_response(self.invalid_param_response)
            return

        self.channels[ch_id]['dc'] = ch_dc
        self.send_response(self.ok_response)

    def mac_set_ch_drrange(self, command):
        if not set_channel_param(command, 7):
            self.send_response(self.invalid_param_response)
            return

        ch_id = int(command[4])
        ch_min_range = int(command[5])
        ch_max_range = int(command[6])

        if not (0 <= ch_min_range <= 7) or (ch_max_range < ch_min_range):
            self.send_response(self.invalid_param_response)

        if not (0 <= ch_max_range <= 7):
            self.send_response(self.invalid_param_response)

        self.channels[ch_id]['min_dr'] = ch_min_range
        self.channels[ch_id]['max_dr'] = ch_max_range

        self.send_response(self.ok_response)

    def mac_set_ch_status(self, command):
        if len(command) != 6:
            self.send_response(self.invalid_param_response)
            return

        try:
            ch_id = int(command[4])
        except ValueError:
            return False

        if not (command[5] == 'on' or command[5] == 'off'):
            self.send_response(self.invalid_param_response)
            return

        ch_status = command[5]

        self.channels[ch_id]['status'] = True if ch_status == 'on' else False
        self.send_response(self.ok_response)

    def mac_set_devaddr(self, command):
        if not set_key(command, 8):
            self.send_response(self.invalid_param_response)
            return

        addr = command[3]
        self.devAddr[0] = addr
        self.send_response(self.ok_response)

    def mac_set_deveui(self, command):
        if not set_key(command, 16):
            self.send_response(self.invalid_param_response)
            return

        deveui = command[3]
        self.devEUI[0] = deveui
        self.send_response(self.ok_response)

    def mac_set_dl_counter(self, command):
        if not set_param(command, 4, 0, 4294967295):
            self.send_response(self.invalid_param_response)
            return

        self.dl_ctr[0] = int(command[3])
        self.send_response(self.ok_response)

    def mac_set_dr(self, command):
        if not set_param(command, 4, 0, 7):
            self.send_response(self.invalid_param_response)
            return

        self.dr[0] = int(command[3])
        self.send_response(self.ok_response)

    def mac_set_nwkskey(self, command):
        if not set_key(command, 32):
            self.send_response(self.invalid_param_response)
            return

        self.nwksKey[0] = command[3]
        self.send_response(self.ok_response)

    def mac_set_pwr_idx(self, command):
        pwr_idx = 0 if self.freq == 433 else 1

        if not set_param(command, 4, pwr_idx, 5):
            self.send_response(self.invalid_param_response)
            return

        self.pwr_idx[0] = int(command[3])
        self.send_response(self.ok_response)

    def mac_set_re_tx(self, command):
        if not set_param(command, 4, 0, 255):
            self.send_response(self.invalid_param_response)
            return

        self.re_tx[0] = int(command[3])
        self.send_response(self.ok_response)

    def mac_set_rx2(self, command):
        if len(command) != 5:
            self.send_response(self.invalid_param_response)
            return

        try:
            dr = int(command[3])
            freq = int(command[4])
        except ValueError:
            self.send_response(self.invalid_param_response)
            return

        if not (433050000 <= freq <= 434790000) and not (863000000 <= freq <= 870000000):
            self.send_response(self.invalid_param_response)
            return

        if not (0 <= dr <= 7):
            self.send_response(self.invalid_param_response)
            return

        self.channels[16]['freq'] = freq
        self.channels[16]['max_dr'] = dr
        self.send_response(self.ok_response)

    def mac_set_rx_delay1(self, command):
        if not set_param(command, 4, 0, 65535):
            self.send_response(self.invalid_param_response)
            return

        self.rx_delay1[0] = int(command[3])
        self.send_response(self.ok_response)

    def mac_set_sync(self, command):
        if not set_key(command, 2):
            self.send_response(self.invalid_param_response)
            return

        self.sync[0] = command[2]
        self.send_response(self.ok_response)

    def mac_set_ul_counter(self, command):
        if not set_param(command, 4, 0, 4294967295):
            self.send_response(self.invalid_param_response)
            return

        self.ul_ctr[0] = int(command[3])
        self.send_response(self.ok_response)

    def mac_get_ar(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return
        self.send_response('on' if self.ar is True else 'off')

    def mac_get_ch_param(self, command, key):
        if set_param(command, 5, 0, 15):
            self.send_response(self.invalid_param_response)
            return

        ch_id = int(command[4])
        if key == 'dr':
            self.send_response("{} {}".format(self.channels[ch_id]['min_dr'], self.channels[ch_id]['max_dr']))
        elif key == 'status':
            self.send_response('on' if self.channels[ch_id]['status'] else 'off')
        else:
            self.send_response(str(self.channels[ch_id][key]))

    def mac_get_devaddr(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(self.devAddr[0])

    def mac_get_deveui(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(self.devEUI[0])

    def mac_get_dncr(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(str(self.dl_ctr[0]))

    def mac_get_dr(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(str(self.dr[0]))

    def mac_get_pwridx(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(str(self.pwr_idx[0]))

    def mac_get_re_tx(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(str(self.re_tx[0]))

    def mac_get_rx2(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response('{} {}'.format(self.channels[16]['max_dr'], self.channels[16]['freq']))

    def mac_get_rx_delay1(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(str(self.rx_delay1[0]))

    def mac_get_rx_delay2(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(str(self.rx_delay2))

    def mac_get_sync(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(self.sync[0])

    def mac_get_upctr(self, command):
        if len(command) != 3:
            self.send_response(self.invalid_param_response)
            return

        self.send_response(str(self.ul_ctr[0]))





































