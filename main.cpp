#include <iostream>
#include "Settings.h"
#include <unistd.h>
#include "rn2483.h"


int main(int, char**)
{
    serial.begin(9600);
    serial.setTimeout(5000);

    int dr = 0;
    std::string DevAddr = "12345678";
    std::string NwkSKey = "12345678123456781234567812345678";
    std::string AppSKey = "12345678123456781234567812345678";
    std::string message = "1234567812345678123456";
                            

    RN2483 rn2483 = RN2483(serial); 
    
    // Check if modem is OK
    std::cout << "[RN2483] Alive check" << std::endl;
    if(rn2483.is_modem_modem_ok() != true) 
    {
        std::cout << "[RN2483] Modem does not work!" << std::endl;
        return 1;
    }
    std::cout << "[RN2483] Modem is OK" << std::endl << std::endl; 

    //---------------------------------------------------------------------
    // ABP activation
    std::cout << "[RN2483] Getting the HWEUI" << std::endl;
    std::string hweui = rn2483.get_hwEUI();
    if(hweui.length() == 0)
    {
        std::cout << "[RN2483] HWEUI not accepted" << std::endl;
        return 1;
    }

    std::cout << "[RN2483] Setting the NwkSKey" << std::endl;
    if(rn2483.set_NwkSKey(NwkSKey) != true)
    {
        std::cout << "[RN2483] NwkSKey not accepted" << std::endl;
        return 1;
    }

    std::cout << "[RN2483] Setting the AppSKey" << std::endl;
    if(rn2483.set_AppSKey(AppSKey) != true)
    {
        std::cout << "[RN2483] AppSKey not accepted" << std::endl;
        return 1;
    }

    std::cout << "[RN2483] Setting the DevAddr" << std::endl;
    if(rn2483.set_DevAddr(DevAddr) != true)
    {
        std::cout << "[RN2483] DevAddr not accepted" << std::endl;
        return 1;
    }

    //---------------------------------------------------------------------
    // Other parameters

    /*
    rn2483.set_ch_freq(0, 868100000);
    rn2483.set_ch_freq(1, 868300000);
    rn2483.set_ch_freq(2, 868500000);
    */
   
    std::cout << "[RN2483] Obtaining the DutyCycle configuration" << std::endl;
    rn2483.get_ch_dc(0);
    rn2483.get_ch_dc(1);
    rn2483.get_ch_dc(2);

    // Disable the dutycycle for the first 3 channels (65535 means unlimited)
    std::cout << "[RN2483] Disabling the DutyCycle" << std::endl;
    for(int i = 0; i < 3; i++)
    {
        if(rn2483.set_ch_dc(i, 65535))
        {
            std::cout << "[RN2483] Error setting the Duty Cycle" << std::endl;
            return 1;
        }
    }
    std::cout << "[RN2483] DutyCycle disabled successfully" << std::endl << std::endl;

    std::cout << "[RN2483] Set DR" << dr << std::endl;
    if(rn2483.set_DataRate(dr) != true)
    {
        std::cout << "[RN2483] DataRate not accepted!" << std::endl;
        return 1;
    }
    //std::cout << "[RN2483] DataRate OK!" << std::endl << std::endl;


    std::cout << "[RN2483] MAC save" << std::endl;
    if(rn2483.mac_save() != true)
    {
        std::cout << "[RN2483] MAC settings not saved" << std::endl;
        return 1;
    }
    std::cout << "[RN2483] MAC saved successfully" << std::endl << std::endl;


    std::cout << "[RN2483] ABP join" << std::endl;
    if(rn2483.join_ABP() != true)
    {
        std::cout << "[RN2483] ABP not joined" << std::endl;
        return 1;
    }
    std::cout << "[RN2483] ABP joined OK" << std::endl << std::endl;



    //---------------------------------------------------------------------
    // MSG sending
    bool confirmed = true;
    
    std::string received_data;

    for(int i = 0; i < 40; i++)
    {
        // Send unconfirmed/confirmed message
        std::cout << "------------------------------------" << std::endl;
        std::cout << "[RN2483] Send Message (" << i << "): " << message << std::endl;
        
        bool result;
        if(confirmed)
        {
            result = rn2483.send_confirmed(message, received_data);
        }
        else
        {
            result = rn2483.send_unconfirmed(message);
        }
        
        if(result != true)
        {
            std::cout << "[RN2483] Message (" << i << ") not sent!" << std::endl << std::endl << std::endl;
            usleep(1*1000000);
            continue;
        }
        
        if(confirmed)
        {
            std::cout << "[RN2483] Received data: " << received_data << std::endl;
        }

        std::cout << "[RN2483] Message (" << i << ") sent OK!" << std::endl;

        usleep(1*1000000);
    }

    std::cout << "------------------------------------" << std::endl;
    std::cout << "Program finished" << std::endl;


    return 0;
}
